import { Component, ChangeDetectorRef, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Platform, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators'
import { from } from 'rxjs';
import { IBeacon } from '@ionic-native/ibeacon/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { async } from '@angular/core/testing';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  data: any[] = [];
  currentLocation = "";
  zone: any;
  currentBeacon: any;
  foundBeacons=[];
  constructor(private http: HttpClient, private nativeHttp: HTTP, private plt: Platform,
    private loadingCtrl: LoadingController, private ibeacon: IBeacon, private localNotifications: LocalNotifications) {
    this.zone = new NgZone({ enableLongStackTrace: false });
    
  }


  async getDataStandard() {
    let loading = await this.loadingCtrl.create()
    await loading.present();

    this.http.get<any[]>('https://warm-badlands-90285.herokuapp.com/restaurants')
      .pipe(
        finalize(() => loading.dismiss())
      )
      .subscribe(data => {
        this.data = data.map(result => result.name);
        console.log(this.data, 'hh')
      }, err => {
        console.log('JS Call error: ', err);
      })

  }
  async getDataNativeHttp() {
    let loading = await this.loadingCtrl.create()
    await loading.present();

    let nativeCall = this.nativeHttp.get('https://warm-badlands-90285.herokuapp.com/restaurants', {}, {
      'Content-Type': 'application/json'
    })
    from(nativeCall).pipe(
      finalize(() => loading.dismiss())
    )
      .subscribe(data => {
        let parsed = JSON.parse(data.data).results;
        this.data = parsed;

        console.log('native data: ', data)
      }, err => {
        console.log('JS Call error: ', err)
      })
  }
  getDataEveryWhere() {
    this.plt.is('cordova') ? this.getDataNativeHttp() : this.getDataStandard();
  }

  async ionViewWillEnter() {
    setInterval(function(){
    this.ScanBeacon()
    }.bind(this),10000)
    
  }

  async ScanBeacon(){
    let loading = await this.loadingCtrl.create()
    await loading.present();

    this.http.get<any[]>('https://warm-badlands-90285.herokuapp.com/restaurants')
      .pipe(
        finalize(() => loading.dismiss())
      )
      .subscribe(data => {
        this.data = data.map(result => result.beacons).map(beacon => beacon[0]);
        for (let item of this.data) {

          let beaconName = item.beaconName;
          let uuid = item.uuid;
          let id = item.id
          let minorId = item.minor
          console.log('list items', uuid)
          console.log('list items', beaconName)
          // ibeacon snippet

          // Request permission to use location on iOS
          this.ibeacon.requestAlwaysAuthorization();
          // create a new delegate and register it with the native layer
          let delegate = this.ibeacon.Delegate();
          // Subscribe to some of the delegate's event handlers
          delegate.didRangeBeaconsInRegion()
            .subscribe(
              beaconData => {
                console.log('didRangeBeaconsInRegion: ', beaconData);
                console.log(beaconData.beacons);
                // this.foundBeacons.push(beaconData.beacons)
                // console.log('foundBeacons = ', this.foundBeacons)
                
                if (beaconData.beacons.length > 0) {
                  console.log("inside");
                  console.log(beaconData.beacons);
                  this.currentBeacon = beaconData.region.identifier;
                  console.log('id ' + this.currentBeacon);
                  console.log(' V1');
                  this, this.zone.run(() => {
                    console.log(' V2');
                    if (beaconData.beacons.length > 0) {
                      console.log(' V3');
                      beaconData.beacons.forEach(beacon => {
                        if (beacon.proximity == 'ProximityImmediate') {
                          console.log('_V4_' + this.currentBeacon);
                          switch (Number(beacon.minor)) {
                            case minorId:
                              this.currentLocation = beaconName;
                              console.log(' V5');
                              break;
                          }
                        }
                      })
                    }
                  })
                }
              
              },
              error => console.error()
            );
          delegate.didStartMonitoringForRegion()
            .subscribe(
              data => console.log('didStartMonitoringForRegion: ', data),
              error => console.error()
            );
          delegate.didDetermineStateForRegion().subscribe(data => {
            let state = data.state
            if (state == 'CLRegionStateInside') {
              this.localNotifications.schedule({
                id: id,
                title: beaconName,
                text: beaconName,
                trigger: { at: new Date(new Date().getTime()) }
              })

            }
          })
          delegate.didEnterRegion()
            .subscribe(
              data => {
                console.log('didEnterRegion: ', data);
                this.localNotifications.schedule({
                  id: id,
                  title: beaconName,
                  text: beaconName,
                  trigger: { at: new Date(new Date().getTime()) }
                })
              }
            );
          let beaconRegion = this.ibeacon.BeaconRegion(beaconName, uuid);

          this.ibeacon.startMonitoringForRegion(beaconRegion)
            .then(
              () => console.log('Native layer received the request to monitoring'),
              error => console.error('Native layer failed to begin monitoring: ', error)
            );
          this.ibeacon.requestStateForRegion(beaconRegion).then(() => console.log('requested state!'))
          this.ibeacon.startRangingBeaconsInRegion(beaconRegion).then(success => {
            console.log('start ranging beacon in region!')
          })
          // end ibeacon snippet
        }
      
        console.log(this.data[0], 'hh')
      }, err => {
        console.log('JS Call error: ', err);
      })
  }
}
